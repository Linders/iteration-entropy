# Install packages
using Pkg
Pkg.activate(@__DIR__)
Pkg.instantiate()


# Load packages
using DelimitedFiles
using LinearAlgebra
using SparseArrays

using ForwardDiff

using SummationByPartsOperators
using Plots: Plots, plot, plot!, savefig


# Newton's method with direct linear solves
function newton!(x, f, J, entropy, line_search, relax, NewtonOpts)
  # Unpack parameters for the iterative solver
  abs_tol_N = NewtonOpts.abstol
  rel_tol_N = NewtonOpts.reltol
  max_N = NewtonOpts.maxiter

  # For book keeping
  Nx = length(x)
  it_count = 0
  residuals = Vector{Float64}()
  entropies = Vector{Float64}()

  # Used for line search and relaxation
  un = copy(x)

  # Evaluate function and norm at initial iterate
  F_val = f(x)
  F_val_norm = norm(F_val) / sqrt(Nx)
  push!(residuals, F_val_norm)
  push!(entropies, entropy(x))
  stop_tol = abs_tol_N + rel_tol_N * F_val_norm

  # Solve nonlinear system using Newton's method
  for ns in 1:max_N
    if F_val_norm < stop_tol
      break
    end

    # Direct linear solve
    dx = -(J(x) \ F_val)

    # Line search
    if line_search
      y = x + dx
      a = -(dot(dx, x) + dot(y, x - un)) / sum(abs2, dx)
      @info "line search parameter" a
      @. x = a * y + (1 - a) * x
    else
      @. x = x + dx
    end

    if relax
      un = hcat(un,x)
    end

    F_val = f(x)
    F_val_norm = norm(F_val) / sqrt(Nx)
    push!(residuals, F_val_norm)
    push!(entropies, entropy(x))
    it_count = ns
  end

  if relax
    return (x, residuals, entropies, un)
  else
    return (x, residuals, entropies)
  end
end

function burgers_main()
  figdir = joinpath(dirname(@__DIR__), "Manuscript", "Pics")

  D1_op = periodic_derivative_operator(
    derivative_order = 1, accuracy_order = 4,
    xmin = -10.0, xmax = 10.0, N = 200)
  D1 = sparse(D1_op)

  # initial data
  x = grid(D1_op)
  c = 2
  u0 = @. 0.5 * c * sech(0.5 * sqrt(c) * x)^2


  # LobattoIIIC method
  let
    # Options for Newton solver
    abstol = 0.0
    reltol = 1.0e-12
    maxiter = 20
    NewtonOpts = (; abstol, reltol, maxiter)

    dt = 0.1

    # Discretization and Jacobian using the three-stage Lobatto IIIC method
    # with Butcher tableau
    #  0  | 1/6 -1/3   1/6
    # 1/2 | 1/6  5/12 -1/12
    #  1  | 1/6  2/3   1/6
    # ---------------------
    #     | 1/6  2/3   1/6
    F(y1, y2, y3, un) = vcat(
      y1 .- un .+ (2 .* dt ./ 6) .* (y1 .* (D1_op * y1) .+ D1_op * (y1.^2)) .- (2 .* dt ./ 3) .* (y2 .* (D1_op * y2) .+ D1_op * (y2.^2)) .+ (2 .* dt ./ 6) .* (y3 .* (D1_op * y3) .+ D1_op * (y3.^2)),
      y2 .- un .+ (2 .* dt ./ 6) .* (y1 .* (D1_op * y1) .+ D1_op * (y1.^2)) .+ (2 .* dt .* 5 ./ 12) .* (y2 .* (D1_op * y2) .+ D1_op * (y2.^2)) .- (2 .* dt ./ 12) .* (y3 .* (D1_op * y3) .+ D1_op * (y3.^2)),
      y3 .- un .+ (2 .* dt ./ 6) .* (y1 .* (D1_op * y1) .+ D1_op * (y1.^2)) .+ (2 .* dt .* 2 ./ 3) .* (y2 .* (D1_op * y2) .+ D1_op * (y2.^2)) .+ (2 .* dt ./ 6) .* (y3 .* (D1_op * y3) .+ D1_op * (y3.^2)),
    )

    # remember that the unknown we feed into Newton's method
    # is the vector of all Runge-Kutta stages
    entropy(y) = 0.5 * integrate(abs2, y[(2*end÷3+1):end], D1_op)

    f(y) = F(y[1:(end÷3)], y[(end÷3+1):(2*end÷3)], y[(2*end÷3+1):end], u0)
    J(y) = ForwardDiff.jacobian(f, y)
    y = vcat(copy(u0), copy(u0), copy(u0))


    (y, residuals, entropies) = newton!(
      y, f, J, entropy, false, false, NewtonOpts)

    open(joinpath(figdir, "burgers__lobatto3c.txt"), "w") do io
      println(io, "# iter\tresidual\tentropy")
      writedlm(io, hcat(0:(length(residuals) - 1), residuals, entropies))
    end
  end


  # Standard implicit midpoint method
  let
    # Options for Newton solver
    abstol = 0.0
    reltol = 1.0e-12
    maxiter = 20
    NewtonOpts = (; abstol, reltol, maxiter)

    # Discretization and Jacobian using implicit midpoint rule
    dt = 0.5
    F(v, vn) = v .- vn .+ dt .* (v .* (D1_op * v) .+ D1_op * (v.^2))
    J(v) = I + dt * (spdiagm(0 => v) * D1 +
                     spdiagm(0 => D1_op * v) +
                     D1 * spdiagm(0 => 2 * v))


    # remember that the unknown we feed into Newton's method
    # is the intermediate stage of the midpoint method
    entropy(U) = 0.5 * integrate(abs2.(2 .* U .- u0), D1_op)

    f(u) = F(u, u0)
    u = copy(u0)
    (u, residuals, entropies) = newton!(
      u, f, J, entropy, false, false, NewtonOpts)

    open(joinpath(figdir, "burgers__midpoint_standard.txt"), "w") do io
      println(io, "# iter\tresidual\tentropy")
      writedlm(io, hcat(0:(length(residuals) - 1), residuals, entropies))
    end
  end


  # Quasi-Newton (modified Jacobian) with implicit midpoint method
  let
    # Options for Newton solver
    abstol = 0.0
    reltol = 1.0e-12
    maxiter = 14
    NewtonOpts = (; abstol, reltol, maxiter)

    # Discretization and Jacobian using implicit midpoint rule
    dt = 0.5
    F(v, vn) = v .- vn .+ dt .* (v .* (D1_op * v) .+ D1_op * (v.^2))
    J(v) = I + dt * (spdiagm(0 => v) * D1 +
                     D1 * spdiagm(0 => v))

    # remember that the unknown we feed into Newton's method
    # is the intermediate stage of the midpoint method
    entropy(U) = 0.5 * integrate(abs2.(2 .* U .- u0), D1_op)

    f(u) = F(u, u0)
    u = copy(u0)
    (u, residuals, entropies) = newton!(
      u, f, J, entropy, false, false, NewtonOpts)

    open(joinpath(figdir, "burgers__midpoint_mod_jacobian.txt"), "w") do io
      println(io, "# iter\tresidual\tentropy")
      writedlm(io, hcat(0:(length(residuals) - 1), residuals, entropies))
    end
  end


  # Inexact Newton (line search) with implicit midpoint method
  let
    # Options for Newton solver
    abstol = 0.0
    reltol = 1.0e-12
    maxiter = 6
    NewtonOpts = (; abstol, reltol, maxiter)

    # Discretization and Jacobian using implicit midpoint rule
    dt = 0.5
    F(v, vn) = v .- vn .+ dt .* (v .* (D1_op * v) .+ D1_op * (v.^2))
    J(v) = I + dt * (spdiagm(0 => v) * D1 +
                     spdiagm(0 => D1_op * v) +
                     D1 * spdiagm(0 => 2 * v))


    # remember that the unknown we feed into Newton's method
    # is the intermediate stage of the midpoint method
    entropy(U) = 0.5 * integrate(abs2.(2 .* U .- u0), D1_op)

    f(u) = F(u, u0)
    u = copy(u0)
    (u, residuals, entropies) = newton!(
      u, f, J, entropy, true, false, NewtonOpts)

    open(joinpath(figdir, "burgers__midpoint_line_search.txt"), "w") do io
      println(io, "# iter\tresidual\tentropy")
      writedlm(io, hcat(0:(length(residuals) - 1), residuals, entropies))
    end
  end


    # Relaxed implicit midpoint method
    let
      # Options for Newton solver
      abstol = 0.0
      reltol = 1.0e-12
      maxiter = 20
      NewtonOpts = (; abstol, reltol, maxiter)
  
      # Discretization and Jacobian using implicit midpoint rule
      dt = 0.5
      F(v, vn, h) = v .- vn .+ h .* (v .* (D1_op * v) .+ D1_op * (v.^2))
      J(v) = I + dt * (spdiagm(0 => v) * D1 +
                       spdiagm(0 => D1_op * v) +
                       D1 * spdiagm(0 => 2 * v))
  
  
      # remember that the unknown we feed into Newton's method
      # is the intermediate stage of the midpoint method
      entropy(U) = 0.5 * integrate(abs2.(2 .* U .- u0), D1_op)
  
      f(u) = F(u, u0, dt)
      u = copy(u0)
      (u, residuals, entropies, U) = newton!(
        u, f, J, entropy, false, true, NewtonOpts)

      # Compute relexation parameter and reconstruct relaxed stage values to obtain relaxed residual
      N = length(u0)
      for iter = 2:length(residuals)
        gamm = (sum(abs2, u0) - dot(U[:,iter], u0)) / sum(abs2, U[:,iter] .- u0)
        Ug = u0 .+ gamm * (U[:,iter] .- u0)
        entropies[iter] = entropy(Ug)
        residuals[iter] = norm( F(Ug, u0, gamm * dt) ) / sqrt(N)
      end
  
      open(joinpath(figdir, "burgers__midpoint_relaxed.txt"), "w") do io
        println(io, "# iter\tresidual\tentropy")
        writedlm(io, hcat(0:(length(residuals) - 1), residuals, entropies))
      end
    end

  @info "Results saved in the directory `figdir`" figdir

  return nothing
end
