# Install packages
using Pkg
Pkg.activate(@__DIR__)
Pkg.instantiate()


# Load packages
using DelimitedFiles
using LinearAlgebra
using SparseArrays

using ForwardDiff

using SummationByPartsOperators
using Plots: Plots, plot, plot!, savefig


include("newton_krylov.jl")


# Semidiscretization of the KdV equation
function getGrid(xRange, N)
    # Get a (periodic) x-axis

    xmin = xRange[1]
    xmax = xRange[2]
    dx = (xmax - xmin) / N
    x = range(xmin, stop=xmax - dx, length=N)

    return (x, dx)
end



function kdvSolver(Nx,time,dt,NewtonOpts,gmresOpts,relax)

    # Generate grid central difference operators
    xRange = [-10,10]
    L = xRange[2] - xRange[1]
    (x,dx) = getGrid(xRange,Nx)

    D1_op = periodic_derivative_operator(derivative_order=1, accuracy_order=4,
                                         xmin=xRange[1], xmax=xRange[2], N=Nx)
    D1 = sparse(D1_op)

    D3_op = periodic_derivative_operator(derivative_order=3, accuracy_order=4,
                                         xmin=xRange[1], xmax=xRange[2], N=Nx)
    D3 = sparse(D3_op)

    Ix = spdiagm(0 => ones(Nx))

    # Exact soliton solution with speed c and starting location x0
    u(x,t,x0,c) = 0.5*c*sech.(0.5*sqrt(c)*(mod.(x .- c*t .- x[1], L) .- x0 .+ x[1])).^2
    x0 = 0
    speed = 2

    # Initial data
    un = u(x,0,x0,speed)

    # Initial entropy and error
    M = round(Int,time/dt)
    ent = zeros(M+1)
    ent[1] = getEntropy(un,dx)
    err = zeros(M+1)

    # Discretization and Jacobian of KdV using implicit midpoint rule
    F(v, vn) = v - vn + dt * (v .* (D1_op * v) + D1_op * (v.^2) + 0.5 * (D3_op * v))
    J(v) = Ix + dt*(spdiagm(0 => v)*D1 + spdiagm(0 => D1*v) + 2.0*D1*spdiagm(0 => v) + 0.5*D3)

    # March in time
    t = zeros(M+1)
    for k = 1:M
        if k % 100 == 0
            println("time = ", t[k])
        end

        f(u) = F(u,un)
        wk = copy(un)
        (wk,res_hist) = newton!(wk,f,J,NewtonOpts,gmresOpts)

        # Relaxation
        α = 1.0
        if relax
            α = (sum(abs2, un) - dot(wk, un)) / sum(abs2, wk - un)
        end
        copyto!(un,2*α*wk + (1-2*α)*un)

        t[k+1] = t[k] + α*dt
        ent[k+1] = getEntropy(un,dx)
        err[k+1] = getError(un,u(x,t[k+1],x0,speed),dx)
    end

    @info "initial error" err[1:5]

    return (x,t,un,ent,err)
end

function kdvSolver_lobatto(Nx, time, dt, NewtonOpts, gmresOpts, relaxation)

    # Generate grid central difference operators
    xRange = [-10,10]
    L = xRange[2] - xRange[1]
    (x,dx) = getGrid(xRange,Nx)

    D1_op = periodic_derivative_operator(derivative_order=1, accuracy_order=4,
                                         xmin=xRange[1], xmax=xRange[2], N=Nx)
    D1 = sparse(D1_op)

    D3_op = periodic_derivative_operator(derivative_order=3, accuracy_order=4,
                                         xmin=xRange[1], xmax=xRange[2], N=Nx)
    D3 = sparse(D3_op)

    Ix = spdiagm(0 => ones(Nx))

    # Exact soliton solution with speed c and starting location x0
    u(x,t,x0,c) = 0.5*c*sech.(0.5*sqrt(c)*(mod.(x .- c*t .- x[1], L) .- x0 .+ x[1])).^2
    x0 = 0
    speed = 2

    # Initial data
    un = u(x,0,x0,speed)

    # Initial entropy and error
    M = round(Int,time/dt)
    ent = zeros(M+1)
    ent[1] = getEntropy(un,dx)
    err = zeros(M+1)

    # Discretization and Jacobian using the three-stage Lobatto IIIC method
    # with Butcher tableau
    #  0  | 1/6 -1/3   1/6
    # 1/2 | 1/6  5/12 -1/12
    #  1  | 1/6  2/3   1/6
    # ---------------------
    #     | 1/6  2/3   1/6
    function F(y1, y2, y3, un)
        rhs_y1 = (y1 .* (D1_op * y1) .+ D1_op * (y1.^2) .+ 0.5 .* (D3_op * y1))
        rhs_y2 = (y2 .* (D1_op * y2) .+ D1_op * (y2.^2) .+ 0.5 .* (D3_op * y2))
        rhs_y3 = (y3 .* (D1_op * y3) .+ D1_op * (y3.^2) .+ 0.5 .* (D3_op * y3))

        res1 = y1 .- un .+ (2 .* dt ./ 6) .* rhs_y1 .- (2 .* dt ./ 3) .* rhs_y2 .+ (2 .* dt ./ 6) .* rhs_y3
        res2 = y2 .- un .+ (2 .* dt ./ 6) .* rhs_y1 .+ (2 .* dt .* 5 ./ 12) .* rhs_y2 .- (2 .* dt ./ 12) .* rhs_y3
        res3 = y3 .- un .+ (2 .* dt ./ 6) .* rhs_y1 .+ (2 .* dt .* 2 ./ 3) .* rhs_y2 .+ (2 .* dt ./ 6) .* rhs_y3
        # Pre-multiply the residual by the ivnerse of the Runge-Kutta matrix A
        #
        # julia> A = [1/6 -1/3 1/6
        #             1/6 5/12 -1/12
        #             1/6 2/3 1/6]
        # 3×3 Matrix{Float64}:
        # 0.166667  -0.333333   0.166667
        # 0.166667   0.416667  -0.0833333
        # 0.166667   0.666667   0.166667
        #
        # julia> inv(A)
        # 3×3 Matrix{Float64}:
        # 3.0   4.0  -1.0
        # -1.0   0.0   1.0
        # 1.0  -4.0   3.0
        return vcat(
            @.(3 * res1 + 4 * res2 - res3),
            @.(-res1 + res3),
            @.(res1 - 4 * res2 + 3 * res3),
        )
    end

    # March in time
    t = zeros(M+1)
    for k = 1:M
        if k % 25 == 0
            println("time = ", t[k])
        end

        f(y) = F(y[1:(end÷3)], y[(end÷3+1):(2*end÷3)], y[(2*end÷3+1):end], un)
        J(y) = ForwardDiff.jacobian(f, y)
        y = vcat(copy(un), copy(un), copy(un))
        (y, res_hist) = newton!(y, f, J, NewtonOpts, gmresOpts)

        if relaxation
            diff = y[(2*end÷3+1):end] - un
            γ = -2 * dot(un, diff) /sum(abs2, diff)
            @. un = un + γ * diff
            t[k+1] = t[k] + γ * dt
        else
            un .= y[(2*end÷3+1):end]
            t[k+1] = t[k] + dt
        end

        ent[k+1] = getEntropy(un,dx)
        err[k+1] = getError(un,u(x,t[k+1],x0,speed),dx)
    end

    @info "initial error" err[1:5]

    return (x,t,un,ent,err)
end

function getEntropy(u,dx)
    # Compute solution entropy
    return dx*norm(u)^2 / 2.0
end

function getError(u,uex,dx)
    # Compute error
    return sqrt(dx)*norm(u - uex)
end


function plot_kwargs()
    fontsizes = (
        xtickfontsize = 14, ytickfontsize = 14,
        xguidefontsize = 16, yguidefontsize = 16,
        legendfontsize = 14)
    return (; linewidth = 4, gridlinewidth = 2, fontsizes...)
end

function kdv_main()
    figdir = joinpath(dirname(@__DIR__), "Manuscript", "Pics")

    fig_error     = plot(xguide = "Time \$t\$", yguide = "\$L^2\$ error"; plot_kwargs()...)
    fig_quadratic = plot(xguide = "Time \$t\$", yguide = "Quadratic invariant"; plot_kwargs()...)

    # Set up problem
    N = 200
    time = 1000.0
    dt = 0.05

    let 
        @info "Implicit midpoint, rel. tolerance 1.0e-3"

        # Options for Newton solver
        abstol = 0
        reltol = 1.0e-3
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = false

        (x, t, u, ent, err) = kdvSolver(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__tol_1em3_absrel.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            # reduce memory pressure for the long time simulation
            idx = 1:5:length(t)
            writedlm(io, hcat(t[idx], ent[idx], err[idx]))
        end
    end

    let 
        @info "Implicit midpoint, rel. tolerance 1.0e-4"

        # Options for Newton solver
        abstol = 0
        reltol = 1.0e-4
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = false

        (x, t, u, ent, err) = kdvSolver(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__tol_1em4_absrel.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            # reduce memory pressure for the long time simulation
            idx = 1:5:length(t)
            writedlm(io, hcat(t[idx], ent[idx], err[idx]))
        end
    end

    let 
        @info "Implicit midpoint, rel. tolerance 1.0e-5"

        # Options for Newton solver
        abstol = 0
        reltol = 1.0e-5
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = false

        (x, t, u, ent, err) = kdvSolver(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__tol_1em5_absrel.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            # reduce memory pressure for the long time simulation
            idx = 1:5:length(t)
            writedlm(io, hcat(t[idx], ent[idx], err[idx]))
        end
    end

    let 
        @info "Implicit midpoint, rel. tolerance 1.0e-3, relaxation"

        # Options for Newton solver
        abstol = 0
        reltol = 1.0e-3
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = true

        (x, t, u, ent, err) = kdvSolver(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__tol_1em3_absrel_relaxation.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            # reduce memory pressure for the long time simulation
            idx = 1:5:length(t)
            writedlm(io, hcat(t[idx], ent[idx], err[idx]))
        end
    end

    savefig(fig_error,     joinpath(figdir, "kdv__error.pdf"))
    savefig(fig_quadratic, joinpath(figdir, "kdv__quadratic.pdf"))

    # Lobatto IIIC method instead of implicit midpoint
    let
        @info "Lobatto IIIC, tolerances 1.0e-3"

        N = 200
        time = 300.0
        dt = 0.1

        # Options for Newton solver
        abstol = 1.0e-3
        reltol = 1.0e-3
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = false

        (x, t, u, ent, err) = kdvSolver_lobatto(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__lobatto3c_tol_1em3_new.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            writedlm(io, hcat(t, ent, err))
        end
    end

    let
        @info "Lobatto IIIC, tolerances 1.0e-4"

        N = 200
        time = 300.0
        dt = 0.1

        # Options for Newton solver
        abstol = 1.0e-4
        reltol = 1.0e-4
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = false

        (x, t, u, ent, err) = kdvSolver_lobatto(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__lobatto3c_tol_1em4_new.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            writedlm(io, hcat(t, ent, err))
        end
    end

    let
        @info "Lobatto IIIC, tolerances 1.0e-5"

        N = 200
        time = 300.0
        dt = 0.1

        # Options for Newton solver
        abstol = 1.0e-5
        reltol = 1.0e-5
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = false

        (x, t, u, ent, err) = kdvSolver_lobatto(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__lobatto3c_tol_1em5_new.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            writedlm(io, hcat(t, ent, err))
        end
    end

    let
        @info "Lobatto IIIC, tolerances 1.0e-3, relaxation"

        N = 200
        time = 300.0
        dt = 0.1

        # Options for Newton solver
        abstol = 1.0e-3
        reltol = 1.0e-3
        maxiter = 10
        NewtonOpts = (; abstol, reltol, maxiter)

        # Options for gmres
        etamax = 0.9
        gamma = 0.9
        maxiter_G = 200
        gmresOpts = (; etamax, maxiter = maxiter_G, gamma)
        # gmresOpts = "none"     # Use Newton with direct linear solver

        # Relaxation
        relax = true

        (x, t, u, ent, err) = kdvSolver_lobatto(N, time, dt, NewtonOpts, gmresOpts, relax)
        open(joinpath(figdir, "kdv__lobatto3c_tol_1em3_relaxation_new.txt"), "w") do io
            println(io, "# t\tquadratic\terror_l2")
            writedlm(io, hcat(t, ent, err))
        end
    end

    @info "Results saved in the directory `figdir`" figdir

    return nothing
end

