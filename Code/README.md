# Numerical experiments

This directory contains all code required to reproduce the numerical
experiments. First, you need to install Julia, e.g., by downloading
the binaries from the [download page](https://julialang.org/downloads/).
The numerical experiments were performed using Julia v1.8.3.


## Burgers' equation

To reproduce the numerical experiments, start Julia and execute the
following commands in the Julia REPL.

```julia
julia> include("burgers.jl")

julia> burgers_main()
```

This will save the generated data in text files in a directory shown
in the REPL.


## Korteweg-de Vries (KdV) equation

To reproduce the numerical experiments, start Julia and execute the
following commands in the Julia REPL.

```julia
julia> include("KdV.jl")

julia> kdv_main()
```

This will save the resulting figures in a directory shown in the REPL.
It will also save the raw data in text files in the same directory.


## Benjamin-Bona-Mahony (BBM) equation

To reproduce the numerical experiments, start Julia and execute the
following commands in the Julia REPL.

```julia
julia> include("bbm.jl")

julia> bbm_main()
```

This will save the resulting figures in a directory shown in the REPL.
It will also save the raw data in text files in the same directory.


## Reproducing the plots
The figures in the paper can be reproduced using the Matlab files (file extension .m).
