LW = 'LineWidth';
lw = 2.5;
MS = 'MarkerSize';
ms = 10;
FS = 'FontSize';
fs = 20;
MFC = 'MarkerFaceColor';
white = [1 1 1];
black = [0 0 0];
gray1  = white*0.9;
gray2  = white*0.7;

filepath = '../Manuscript/Pics/';

% Load data
wid = 'MATLAB:table:ModifiedAndSavedVarnames';
warning('off',wid)
M30 = readtable([filepath,'kdv__tol_1em3_absrel.txt']);
M40 = readtable([filepath,'kdv__tol_1em4_absrel.txt']);
M50 = readtable([filepath,'kdv__tol_1em5_absrel.txt']);
M3r = readtable([filepath,'kdv__tol_1em3_absrel_relaxation.txt']);
warning('on',wid)

% L2 error
sp1 = subplot(1,2,1);
hold on
plot(M30.x_T,M30.error_l2,'-','color',gray1, LW,lw,MS,ms,MFC,white)
plot(M40.x_T,M40.error_l2,'-','color',gray2,LW,lw,MS,ms,MFC,white)
plot(M50.x_T,M50.error_l2,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(M3r.x_T,M3r.error_l2,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('$L^2$ error',FS,fs)
grid on

hold off


% Entropy error
M30ee = M30.quadratic - M30.quadratic(1);
M40ee = M40.quadratic - M40.quadratic(1);
M50ee = M50.quadratic - M50.quadratic(1);
M3ree = M3r.quadratic - M3r.quadratic(1);

sp2 = subplot(1,2,2);
hold on
plot(M30.x_T,M30ee,'-','color',gray1, LW,lw,MS,ms,MFC,white)
plot(M40.x_T,M40ee,'-','color',gray2,LW,lw,MS,ms,MFC,white)
plot(M50.x_T,M50ee,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(M3r.x_T,M3ree,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('Entropy error',FS,fs)
grid on
axis([0 1000 -2e-4 12e-3])
%axis([0 1000 -2e-4 2e-3])

fig = gcf;
fig.Position(3) = 1100;

% {
lgnd = legend({'Tolerance $10^{-3}$', ...
               'Tolerance $10^{-4}$', ...
               'Tolerance $10^{-5}$', ...
               'Tolerance $10^{-3}$, relaxation'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
lgnd.Orientation = 'Horizontal';
lgnd.NumColumns = 2;
lgnd.Location = 'northoutside';
drawnow;

axisHeight = sp2.Position(4);
lgnd.Position(1) = 0.25;
sp1.Position(4) = axisHeight;
sp2.Position(4) = axisHeight;
%}
hold off






