LW = 'LineWidth';
lw = 2.5;
MS = 'MarkerSize';
ms = 10;
FS = 'FontSize';
fs = 20;
MFC = 'MarkerFaceColor';
white = [1 1 1];
black = [0 0 0];
gray  = white*0.7;

filepath = '../Manuscript/Pics/';

%
% Quadratic invariant
%

% Load data
wid = 'MATLAB:table:ModifiedAndSavedVarnames';
warning('off',wid)
Q30 = readtable([filepath,'bbm_quadratic__tol_1em3.txt']);
Q40 = readtable([filepath,'bbm_quadratic__tol_1em4.txt']);
Q3r = readtable([filepath,'bbm_quadratic__tol_1em3_relaxation.txt']);
warning('on',wid)

% L2 error
figure(1)
sp1 = subplot(1,2,1);
hold on
plot(Q30.x_T,Q30.error_l2,'-','color',gray, LW,lw,MS,ms,MFC,white)
plot(Q40.x_T,Q40.error_l2,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(Q3r.x_T,Q3r.error_l2,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('$L^2$ error',FS,fs)
grid on

hold off


% Entropy error
Q30ee = Q30.quadratic - Q30.quadratic(1);
Q40ee = Q40.quadratic - Q40.quadratic(1);
Q3ree = Q3r.quadratic - Q3r.quadratic(1);

sp2 = subplot(1,2,2);
hold on
plot(Q30.x_T,Q30ee,'-','color',gray, LW,lw,MS,ms,MFC,white)
plot(Q40.x_T,Q40ee,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(Q3r.x_T,Q3ree,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('Entropy error',FS,fs)
grid on
ylim([-0.14 0.01])

fig = gcf;
fig.Position(3) = 1100;

% {
lgnd = legend({'Tolerance $10^{-3}$', ...
               'Tolerance $10^{-4}$', ...
               'Tolerance $10^{-3}$, relaxation'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
lgnd.Orientation = 'Horizontal';
lgnd.NumColumns = 3;
lgnd.Location = 'northoutside';
drawnow;

axisHeight = sp2.Position(4);
lgnd.Position(1) = 0.15;
sp1.Position(4) = axisHeight;
sp2.Position(4) = axisHeight;
%}
hold off


%
% Cubic invariant
%

% Load data
wid = 'MATLAB:table:ModifiedAndSavedVarnames';
warning('off',wid)
C30 = readtable([filepath,'bbm_cubic__tol_1em3.txt']);
C40 = readtable([filepath,'bbm_cubic__tol_1em4.txt']);
C3r = readtable([filepath,'bbm_cubic__tol_1em3_relaxation.txt']);
warning('on',wid)

% L2 error
figure(2)
sp1 = subplot(1,2,1);
hold on
plot(C30.x_T,C30.error_l2,'-','color',gray, LW,lw,MS,ms,MFC,white)
plot(C40.x_T,C40.error_l2,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(C3r.x_T,C3r.error_l2,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('$L^2$ error',FS,fs)
grid on

hold off


% Entropy error
C30ee = C30.quadratic - C30.quadratic(1);
C40ee = C40.quadratic - C40.quadratic(1);
C3ree = C3r.quadratic - C3r.quadratic(1);

sp2 = subplot(1,2,2);
hold on
plot(C30.x_T,C30ee,'-','color',gray, LW,lw,MS,ms,MFC,white)
plot(C40.x_T,C40ee,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(C3r.x_T,C3ree,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('Entropy error',FS,fs)
grid on
ylim([-0.14 0.01])

fig = gcf;
fig.Position(3) = 1100;

% {
lgnd = legend({'Tolerance $10^{-3}$', ...
               'Tolerance $10^{-4}$', ...
               'Tolerance $10^{-3}$, relaxation'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
lgnd.Orientation = 'Horizontal';
lgnd.NumColumns = 3;
lgnd.Location = 'northoutside';
drawnow;

axisHeight = sp2.Position(4);
lgnd.Position(1) = 0.15;
sp1.Position(4) = axisHeight;
sp2.Position(4) = axisHeight;
%}
hold off






