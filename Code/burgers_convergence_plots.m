

LW = 'LineWidth';
lw = 2.5;
MS = 'MarkerSize';
ms = 10;
FS = 'FontSize';
fs = 20;
MFC = 'MarkerFaceColor';
mfc = [1 1 1];
gray = [0.5 0.5 0.5];
left_color = [0 0 0];
right_color = [0 0 0];



filepath = '~/Lund/Research/Entropy/entropy-stable-iterative-methods/Manuscript/Pics/';
% Load data
wid = 'MATLAB:table:ModifiedAndSavedVarnames';
warning('off',wid)

NL = readtable([filepath,'burgers__lobatto3c.txt']);
NM = readtable([filepath,'burgers__midpoint_standard.txt']);
NQ = readtable([filepath,'burgers__midpoint_mod_jacobian.txt']);
NI = readtable([filepath,'burgers__midpoint_line_search.txt']);
NR = readtable([filepath,'burgers__midpoint_relaxed.txt']);
warning('on',wid)

TA = ...
    array2table( [1, 0.9221026906287468
                  2, 0.8463069854241382
                  3, 0.901830684906723
                  4, 0.945766198754151
                  5, 0.9533578162878713
                  6, 0.953805969268667], ...
                  'VariableNames',{'iter','alpha'} );

% Lobatto, regular Newton
fig1 = figure(1);
set(fig1,'defaultAxesColorOrder',[left_color; right_color]);

yyaxis right
semilogy(NL.x_Iter,NL.residual,'-o','color',gray,LW,lw,MS,ms,MFC,mfc)
xlim([-0.5 4.5]);
ylim([1e-20 1]);
ylabel('Residual',FS,fs)

ytr = get(gca, 'YTick');                            % Right ticks

yyaxis left
plot(NL.x_Iter,NL.entropy,'k-o',LW,lw,MS,ms,MFC,mfc)
xlim([-0.5 4.5]);
ylim([0.9425 0.944]);
xlabel('Iteration, $k$',FS,fs)
ylabel('Entropy',FS,fs)

ytl = get(gca, 'YTick');                            % Left ticks
ytlv = linspace(min(ytl), max(ytl), numel(ytr));    % Tick increment
ytlc = compose('%.5f',ytlv);                        % Tick label cell
set(gca, 'YTick',ytlv, 'YTickLabel',ytlc)

lgnd = legend({'Entropy', 'Residual'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
grid on


% Midpoint, regular Newton
fig2 = figure(2);
set(fig2,'defaultAxesColorOrder',[left_color; right_color]);

yyaxis right
semilogy(NM.x_Iter,NM.residual,'-o','color',gray,LW,lw,MS,ms,MFC,mfc)
xlim([-0.5 5.5]);
ylim([1e-15 1]);
ylabel('Residual',FS,fs)

ytr = get(gca, 'YTick');                            % Right ticks

yyaxis left
plot(NM.x_Iter,NM.entropy,'k-o',LW,lw,MS,ms,MFC,mfc)
xlim([-0.5 5.5]);
ylim([0.94 0.98]);
xlabel('Iteration, $k$',FS,fs)
ylabel('Entropy',FS,fs)

ytl = get(gca, 'YTick');                            % Left ticks
ytlv = linspace(min(ytl), max(ytl), numel(ytr));    % Tick increment
ytlc = compose('%.3f',ytlv);                        % Tick label cell
set(gca, 'YTick',ytlv, 'YTickLabel',ytlc)

lgnd = legend({'Entropy', 'Residual'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
grid on


% Midpoint, quasi-Newton
fig3 = figure(3);
set(fig3,'defaultAxesColorOrder',[left_color; right_color]);

yyaxis right
semilogy(NQ.x_Iter,NQ.residual,'-o','color',gray,LW,lw,MS,ms,MFC,mfc)
xlim([-1 15]);
ylim([1e-6 1]);
ylabel('Residual',FS,fs)

ytr = get(gca, 'YTick');                            % Right ticks

yyaxis left
plot(NQ.x_Iter,NQ.entropy,'k-o',LW,lw,MS,ms,MFC,mfc)
xlim([-1 15]);
ylim([0.9428 0.9429]);
xlabel('Iteration, $k$',FS,fs)
ylabel('Entropy',FS,fs)

ytl = get(gca, 'YTick');                            % Left ticks
ytlv = linspace(min(ytl), max(ytl), numel(ytr));    % Tick increment
ytlc = compose('%.5f',ytlv);                        % Tick label cell
set(gca, 'YTick',ytlv, 'YTickLabel',ytlc)

lgnd = legend({'Entropy', 'Residual'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
grid on


% Midpoint, inexact Newton
fig4 = figure(4);
set(fig4,'defaultAxesColorOrder',[left_color; right_color]);

yyaxis right
semilogy(NI.x_Iter,NI.residual,'-o','color',gray,LW,lw,MS,ms,MFC,mfc)
xlim([-1 7]);
ylim([1e-8 1]);
ylabel('Residual',FS,fs)

ytr = get(gca, 'YTick');                            % Right ticks

yyaxis left
plot(NI.x_Iter,NI.entropy,'k-o',LW,lw,MS,ms,MFC,mfc)
xlim([-1 7]);
ylim([0.9428 0.9429]);
xlabel('Iteration, $k$',FS,fs)
ylabel('Entropy',FS,fs)

ytl = get(gca, 'YTick');                            % Left ticks
ytlv = linspace(min(ytl), max(ytl), numel(ytr));    % Tick increment
ytlc = compose('%.5f',ytlv);                        % Tick label cell
set(gca, 'YTick',ytlv, 'YTickLabel',ytlc)

lgnd = legend({'Entropy', 'Residual'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
grid on

% Relaxation parameter
axs = axes('Position',[0.27 .3535 .25 .22]);
hold(axs,'on')
box on
plot(TA.iter,TA.alpha,'k-o',LW,2,MS,7,MFC,mfc)
axis([0.5 6.5 0.8 1.0])
grid on
xlabel('Iteration, $k$',FS,16)
ylabel('$\alpha_k$',FS,16)


% Midpoint, relaxed
fig5 = figure(5);
set(fig5,'defaultAxesColorOrder',[left_color; right_color]);

yyaxis right
semilogy(NR.x_Iter,NR.residual,'-o','color',gray,LW,lw,MS,ms,MFC,mfc)
xlim([-0.5 5.5]);
ylim([1e-15 1]);
ylabel('Residual',FS,fs)

ytr = get(gca, 'YTick');                            % Right ticks

yyaxis left
plot(NR.x_Iter,NR.entropy,'k-o',LW,lw,MS,ms,MFC,mfc)
xlim([-0.5 5.5]);
ylim([0.94 0.98]);
xlabel('Iteration, $k$',FS,fs)
ylabel('Entropy',FS,fs)

ytl = get(gca, 'YTick');                            % Left ticks
ytlv = linspace(min(ytl), max(ytl), numel(ytr));    % Tick increment
ytlc = compose('%.3f',ytlv);                        % Tick label cell
set(gca, 'YTick',ytlv, 'YTickLabel',ytlc)

lgnd = legend({'Entropy', 'Residual'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
grid on


