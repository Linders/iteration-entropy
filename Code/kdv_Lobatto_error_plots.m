LW = 'LineWidth';
lw = 2.5;
MS = 'MarkerSize';
ms = 10;
FS = 'FontSize';
fs = 20;
MFC = 'MarkerFaceColor';
white = [1 1 1];
black = [0 0 0];
gray1  = white*0.9;
gray2  = white*0.7;

filepath = '../Manuscript/Pics/';

% Load data
wid = 'MATLAB:table:ModifiedAndSavedVarnames';
warning('off',wid)
L30 = readtable([filepath,'kdv__lobatto3c_tol_1em3_new.txt']);
L40 = readtable([filepath,'kdv__lobatto3c_tol_1em4_new.txt']);
L50 = readtable([filepath,'kdv__lobatto3c_tol_1em5_new.txt']);
L3r = readtable([filepath,'kdv__lobatto3c_tol_1em3_relaxation_new.txt']);
warning('on',wid)

% L2 error
sp1 = subplot(1,2,1);
hold on
plot(L30.x_T,L30.error_l2,'-','color',gray1, LW,lw,MS,ms,MFC,white)
plot(L40.x_T,L40.error_l2,'-','color',gray2,LW,lw,MS,ms,MFC,white)
plot(L50.x_T,L50.error_l2,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(L3r.x_T,L3r.error_l2,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('$L^2$ error',FS,fs)
grid on

hold off


% Entropy error
L30ee = L30.quadratic - L30.quadratic(1);
L40ee = L40.quadratic - L40.quadratic(1);
L50ee = L50.quadratic - L50.quadratic(1);
L3ree = L3r.quadratic - L3r.quadratic(1);

sp2 = subplot(1,2,2);
hold on
plot(L30.x_T,L30ee,'-','color',gray1, LW,lw,MS,ms,MFC,white)
plot(L40.x_T,L40ee,'-','color',gray2,LW,lw,MS,ms,MFC,white)
plot(L50.x_T,L50ee,'-','color',black,LW,lw,MS,ms,MFC,white)
plot(L3r.x_T,L3ree,':','color',black,LW,lw,MS,ms,MFC,white)
xlabel('Time',FS,fs)
ylabel('Entropy error',FS,fs)
grid on
axis([0 300 -3e-2 3e-2])

fig = gcf;
fig.Position(3) = 1100;

% {
lgnd = legend({'Tolerance $10^{-3}$', ...
               'Tolerance $10^{-4}$', ...
               'Tolerance $10^{-5}$', ...
               'Tolerance $10^{-3}$, relaxation'});
lgnd.BoxFace.ColorType='truecoloralpha';
lgnd.BoxFace.ColorData=uint8(255*[1 1 1 0.75]');
lgnd.Orientation = 'Horizontal';
lgnd.NumColumns = 2;
lgnd.Location = 'northoutside';
drawnow;

axisHeight = sp2.Position(4);
lgnd.Position(1) = 0.25;
sp1.Position(4) = axisHeight;
sp2.Position(4) = axisHeight;
%}
hold off
