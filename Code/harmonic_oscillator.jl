# Install packages
using Pkg
Pkg.activate(@__DIR__)
Pkg.instantiate()


# Load packages
using LinearAlgebra

using StaticArrays
using Plots: Plots, plot, plot!, savefig


function usol(t)
  si, co = sincos(t)
  return SVector(co, si)
end

function osc_solver(; dt, perturbation)
  tspan = (0.0, 4 * π)

  # The implicit midpoint method applied to the linear problem
  # u'(t) = L * u(t)
  # is given by the iteration
  # u -> inv(I - dt / 2 * L) * (I + dt / 2 * L) * u
  # For the harmonic oscillator,
  # L = [0 -1; 1 0]
  # and we can compute everything explicitly.
  L = @SArray [0 -1; 1 0]
  a = dt / 2
  operator = (I - a * L) \ (I + a * L)

  t = first(tspan)
  u = usol(t)
  max_deviation_gamma = 0.0
  while t < last(tspan)
    # the operator constructed above uses a fixedtime step
    # if t + dt > last(tspan)
    #   dt = last(tspan) - t
    # end

    # one time step of the baseline method
    unew = operator * u

    # perturbation
    si, co = sincos(2π * rand())
    unew = unew + perturbation * SVector(co, si)

    if unew == u
      u = unew
      t = t + dt
    else
      # relaxation for the inner product norm
      γ = -2 * dot(u, unew - u) / sum(abs2, unew - u)
      max_deviation_gamma = max(max_deviation_gamma, abs(γ - 1))
      u = u + γ * (unew - u)
      # use an IDT step at the very end
      if dt == (last(tspan) - t)
        t = t + dt
      else
        t = t + γ * dt
      end
    end
  end

  error_l2 = norm(u - usol(t))

  return (; t, u, error_l2, max_deviation_gamma)
end


function plot_kwargs()
  fontsizes = (
      xtickfontsize = 14, ytickfontsize = 14,
      xguidefontsize = 16, yguidefontsize = 16,
      legendfontsize = 14)
  return (; linewidth = 4, gridlinewidth = 2, fontsizes...)
end

function osc_main()
  fig = plot(xguide = "Time step \$\\Delta t\$",
             yguide = "Error at \$t = 4 \\pi\$",
             xscale = :log10, yscale = :log10, legend = :bottomright)
  fig_gamma = plot(xguide = "Time step \$\\Delta t\$",
             yguide = "Maximal \$|\\gamma - 1|\$",
             xscale = :log10, yscale = :log10, legend = :topright)
  step_sizes = 10.0 .^ range(-1, -5, length = 21)
  err = zeros(length(step_sizes))
  gamma_err = zeros(length(step_sizes))

  for (i, dt) in enumerate(step_sizes)
    res = osc_solver(; dt = dt, perturbation = dt^1)
    err[i] = res.error_l2
    gamma_err[i] = res.max_deviation_gamma
  end
  plot!(fig, step_sizes, err, label = "\$\\varepsilon = \\Delta t\$";
        plot_kwargs()...)
  plot!(fig_gamma, step_sizes, gamma_err, label = "\$\\varepsilon = \\Delta t\$";
        plot_kwargs()...)

  for (i, dt) in enumerate(step_sizes)
    res = osc_solver(; dt = dt, perturbation = dt^2)
    err[i] = res.error_l2
    gamma_err[i] = res.max_deviation_gamma
  end
  plot!(fig, step_sizes, err, label = "\$\\varepsilon = \\Delta t^2\$";
        plot_kwargs()...)
  plot!(fig_gamma, step_sizes, gamma_err, label = "\$\\varepsilon = \\Delta t^2\$";
        plot_kwargs()...)

  for (i, dt) in enumerate(step_sizes)
    res = osc_solver(; dt = dt, perturbation = dt^3)
    err[i] = res.error_l2
    gamma_err[i] = res.max_deviation_gamma
  end
  plot!(fig, step_sizes, err, label = "\$\\varepsilon = \\Delta t^3\$";
        plot_kwargs()...)
  plot!(fig_gamma, step_sizes, gamma_err, label = "\$\\varepsilon = \\Delta t^3\$";
        plot_kwargs()...)

  for (i, dt) in enumerate(step_sizes)
    res = osc_solver(; dt = dt, perturbation = 0)
    err[i] = res.error_l2
  end
  plot!(fig, step_sizes, err, label = "\$\\varepsilon = 0\$";
        plot_kwargs()...)

  plot!(fig, step_sizes, err[1] ./ step_sizes[1]^2 .* step_sizes .^ 2,
        color = "gray", linestyle = :dash, label = "Order 2";
        plot_kwargs()...)
  plot!(fig, step_sizes, err[1] ./ step_sizes[1]^1 .* step_sizes .^ 1,
        color = "gray", linestyle = :dot, label = "Order 1";
        plot_kwargs()...)

  plot!(fig_gamma, step_sizes, gamma_err[1] ./ step_sizes[1]^1 .* step_sizes .^ 1,
        color = "gray", linestyle = :dot, label = "Order 1";
        plot_kwargs()...)

  figdir = joinpath(dirname(@__DIR__), "Manuscript", "Pics")
  savefig(fig, joinpath(figdir, "osc__error.pdf"))
  savefig(fig_gamma, joinpath(figdir, "osc__gamma.pdf"))

  return nothing
end
