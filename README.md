# Resolving Entropy Growth from Iterative Methods

[![License: MIT](https://img.shields.io/badge/License-MIT-success.svg)](https://opensource.org/licenses/MIT)
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7669546.svg)](https://doi.org/10.5281/zenodo.7669546)

This repository contains information and code to reproduce the results presented in the article

```
@article{linders2023resolving,
  title={Resolving Entropy Growth from Iterative Methods},
  author={Linders, Viktor and Ranocha, Hendrik and Birken, Philipp},
  journal={Bit Numer. Math.},
  volume={63},
  number={45},
  year={2023},
  doi={10.1007/s10543-023-00992-w}
}
```

If you find these results useful, please cite the article mentioned above. If you
use the implementations provided here, please **also** cite this repository as
```
@misc{linders2023resolvingRepro,
  title={Reproducibility repository for
         "{R}esolving Entropy Growth from Iterative Methods"},
  author={Linders, Viktor and Ranocha, Hendrik and Birken, Philipp},
  year={2023},
  howpublished={\url{https://gitlab.maths.lu.se/Linders/iteration-entropy}},
  doi={10.5281/zenodo.7669546}
}
```


## Abstract

We consider entropy conservative and dissipative discretizations of nonlinear conservation laws with implicit time discretizations and investigate the influence of iterative methods used to solve the arising nonlinear equations. We show that Newton's method can turn an entropy dissipative scheme into an anti-dissipative one, even when the iteration error is smaller than the time integration error. We explore several remedies, of which the most performant is a relaxation technique, originally designed to fix entropy errors in time integration methods. Thus, relaxation works well in consort with iterative solvers, provided that the iteration errors are on the order of the time integration method. To corroborate our findings, we consider Burgers' equation and nonlinear dispersive wave equations. We find that entropy conservation results in more accurate numerical solutions than non-conservative schemes, even when the tolerance is an order of magnitude larger.


## Numerical experiments

To reproduce the numerical experiments, you need to install [Julia](https://julialang.org/).
Running the plot scripts additionally requires [Matlab](https://se.mathworks.com/products/matlab.html).

The `Code` directory contains Julia and Matlab code and instructions to reproduce the numerical experiments.

The numerical experiments were carried out using Julia v1.8.3.

## Authors

* [Viktor Linders](https://www.maths.lu.se/staff/viktor-linders/) (University of Lund, Sweden)
* [Hendrik Ranocha](https://ranocha.de) (University of Hamburg, Germany)
* [Philipp Birken](https://www.maths.lu.se/staff/philipp-birken/) (University of Lund, Sweden)


## Disclaimer

Everything is provided as is and without warranty. Use at your own risk!
